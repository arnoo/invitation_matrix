const urlParams = new URLSearchParams(window.location.search);

const from = urlParams.get('de');
const from_address = urlParams.get('de_adresse');
const room = '"'+urlParams.get('salon')+'"';

for (let e of document.getElementsByClassName('from')) {
    e.textContent = from
}
for (let e of document.getElementsByClassName('with_from')) {
    e.textContent = "avec "+from 
}
for (let e of document.getElementsByClassName('from_address')) {
    e.textContent = from_address
}
if (urlParams.has("salon")) {
    for (let e of document.getElementsByClassName('send_pm')) {
        e.style.display = 'none';
    }
    for (let e of document.getElementsByClassName('room')) {
        e.textContent = room
    }
    for (let e of document.getElementsByClassName('room_on')) {
        e.textContent = room+" sur"
    }
} else {
    for (let e of document.getElementsByClassName('join_room')) {
        e.style.display = 'none';
    }
}
document.body.style.display='block';
